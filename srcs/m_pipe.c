/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_pipe.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 12:04:43 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/23 11:35:01 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static bool			add_pipe(char **tab, t_data *data)
{
	t_pipe			*tmp;
	t_pipe			*node;

	if (!(node = (t_pipe *)malloc(sizeof(t_pipe) * 1)))
		return (false);
	node->in = ft_strdup(tab[0]);
	node->out = ft_strdup(tab[1]);
	node->next = NULL;
	if (data->pipes == NULL)
	{
		data->pipes = node;
		data->nb_link++;
		return (true);
	}
	tmp = data->pipes;
	while (tmp->next != NULL)
		tmp = tmp->next;
	tmp->next = node;
	data->nb_link++;
	return (true);
}

bool				p_pipe(char *line, t_data *data)
{
	int				ret;

	ret = 0;
	ret = ft_strnchr(line, '-');
	if (ret > 1 || ret < 1 || data->nb_room < 1)
		return (false);
	if (data->next_type != NEUTRE)
		return (false);
	return (true);
}

static bool			check_cnt(char *s, t_data *data)
{
	t_room			*tmp;

	if (data->rooms == NULL)
		return (true);
	tmp = data->rooms;
	while (tmp->next != NULL)
	{
		if (ft_strequ(s, tmp->name))
			return (true);
		tmp = tmp->next;
	}
	if (ft_strequ(s, tmp->name))
		return (true);
	return (false);
}

bool				m_pipe(char *line, t_data *data)
{
	int				i;
	int				ret;
	char			**tab;

	i = 0;
	ret = 0;
	tab = ft_strsplit(line, '-');
	while (tab && tab[i])
		i++;
	if (i != 2 || !check_cnt(tab[0], data) || !check_cnt(tab[1], data))
		ret = 1;
	if (ret != 1 && add_pipe(tab, data) == false)
		ret = 1;
	(tab) ? ft_str_tabdel(&tab) : 0;
	return ((ret == 0) ? true : false);
}
