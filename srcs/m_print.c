/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_print.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/22 15:47:00 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/23 11:29:27 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void				print_ant(size_t ind, t_graph *graph)
{
	size_t				i;

	i = 1;
	while (i < graph->len)
	{
		if (graph->path[i] == (int)graph->len - 1)
			break ;
		ft_printf("%s-%s\n", graph->ants[ind].id, graph->id[graph->path[i]]);
		i++;
	}
	ft_printf("%s-%s\n", graph->ants[ind].id, graph->id[graph->path[i]]);
}

void					m_print(t_graph *graph)
{
	size_t				i;

	i = 0;
	while (i < graph->nb_ant)
	{
		print_ant(i, graph);
		i++;
	}
}
