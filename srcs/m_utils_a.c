/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_utils_a.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 14:58:59 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/22 16:26:52 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void				ft_stock(char *line, t_data *data)
{
	data->readed = ft_strjoin_free(data->readed, line, 1);
	data->readed = ft_strjoin_free(data->readed, "\n", 1);
}

bool				ft_is_overflow(char *line)
{
	long long		n;

	n = ft_atoll(line);
	if (n < -2146473648 || n > 2146473647)
		return (true);
	return (false);
}

bool				ft_is_str_n(char *line)
{
	int				i;

	i = 0;
	while (line[i])
	{
		if (ft_isdigit(line[i]) == 0)
			return (false);
		i++;
	}
	return (true);
}

static void			clean_graph(int ***as, size_t len, t_graph *graph)
{
	size_t			i;

	i = 0;
	if (!as || len < 1 || !graph->ants)
		return ;
	while (i < len)
	{
		ft_int_tabdel(&(*as)[i]);
		i++;
	}
	i = 0;
	while (i < graph->nb_ant)
	{
		ft_strdel(&graph->ants[i].id);
		i++;
	}
	free(graph->ants);
	free((*as));
	(*as) = NULL;
}

void				ft_message(int i, t_data *data, t_graph *graph)
{
	if (data)
	{
		if (data->rooms)
			ft_kill_room(data->rooms);
		if (data->pipes)
			ft_kill_pipe(data->pipes);
		if (data->readed != NULL)
			ft_strdel(&data->readed);
	}
	if (graph)
	{
		if (graph->matr != NULL)
			clean_graph(&graph->matr, graph->len, graph);
		if (graph->id != NULL)
			ft_str_tabdel(&graph->id);
		if (graph->path != NULL)
			ft_int_tabdel(&graph->path);
	}
	if (i == 2)
	{
		ft_putendl("ERROR");
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}
