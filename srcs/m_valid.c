/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_valid.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/20 17:10:35 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/22 16:05:21 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void			lst_swap(t_room *head, t_room *node)
{
	char			*tmp1;
	char			*tmp2;

	tmp1 = ft_strdup(head->name);
	tmp2 = ft_strdup(node->name);
	ft_strdel(&head->name);
	ft_strdel(&node->name);
	head->name = ft_strdup(tmp2);
	node->name = ft_strdup(tmp1);
	head->type = node->type;
	node->type = NEUTRE;
	ft_strdel(&tmp1);
	ft_strdel(&tmp2);
}

static void			start_at_head(t_data *data)
{
	t_room			*tmp;

	tmp = data->rooms;
	if (data->rooms->type == START)
		return ;
	while (tmp->next)
	{
		if (tmp->type == START)
			break ;
		tmp = tmp->next;
	}
	lst_swap(data->rooms, tmp);
}

static void			start_at_end(t_data *data)
{
	t_room			*tmp;
	t_room			*tail;

	tmp = data->rooms;
	tail = data->rooms;
	while (tail->next)
		tail = tail->next;
	if (tail->type == END)
		return ;
	while (tmp->next)
	{
		if (tmp->type == END)
			break ;
		tmp = tmp->next;
	}
	lst_swap(tail, tmp);
}

bool				m_valid(t_data *data)
{
	if (!data->rooms || !data->pipes || data->get_start == false || \
			data->get_end == false || data->nb_room < 2)
		return (false);
	start_at_head(data);
	start_at_end(data);
	return (true);
}
